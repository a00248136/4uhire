package com.ait.vehicle;
import java.util.ArrayList;
import java.util.Scanner;

public class Vehicle
{
	private String registration;
	private String brand;
	private String name;
	private String category;

	public String getRegistration() {
		return registration;
	}

	public Vehicle setRegistration(String registration) {
		this.registration = registration;
		return this;
	}

	public String getBrand() {
		return brand;
	}

	public Vehicle setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public String getName() {
		return name;
	}

	public Vehicle setName(String name) {
		this.name = name;
		return this;
	}

	public String getCategory() {
		return category;
	}

	public Vehicle setCategory(String category) {
		this.category = category;
		return this;
	}

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
	    // Create an array list vehicles
	    ArrayList<String> vehicles = new ArrayList<String>();
		
		// Program should run until choice isn't equal to "y" or "Y"
        String choice = "y";
        while (choice.equalsIgnoreCase("y"))
        {    		
    		// Create an object v
    		Vehicle v = new Vehicle();
    		
			System.out.print("Enter registration: ");
			String newRegistration = sc.next();
			
			System.out.print("Enter brand: ");
			String newBrand = sc.next();
			
			System.out.print("Enter name: ");
			String newName = sc.next();
			
			System.out.print("Enter vehcile category: ");
			String newCategory = sc.next();
			
			v.setRegistration(newRegistration);
			v.setBrand(newBrand);
			v.setName(newName);
			v.setCategory(newCategory);
			
			vehicles.add(v.getRegistration());
			vehicles.add(v.getBrand());
			vehicles.add(v.getName());
			vehicles.add(v.getCategory());
			
			// see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
        
    	// Display overall results if user chooses not to continue
        if (choice.equalsIgnoreCase("n"))
        {
    		// Display results to user
        	System.out.println(vehicles);
        }
	}
}
