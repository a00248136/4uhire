package com.ait.admin;

//import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ManagerLogin implements Serializable {

	//Declare instance variabless
		private static final long serialVersionUID = 1L;
		private ManagementStaffMethods managementDB;
		private String ID;
		private String password;
		
		//instantiate an object of management staff
		//@PostConstruct
		public ManagerLogin(){
			managementDB = new ManagementStaffMethods();
			this.ID = "";
			this.password = "";				
		}
		
		//Return Id
		public String getID() {
			return ID;
		}
		
		//Set id
		public void setID(String username) {
			this.ID = username.toLowerCase();
		}
		
		//Get password
		public String getPassword() {
			return password;
		}
		
		//Set password
		public void setPassword(String password) {
			this.password = password;
		}
		
		//Check UserID
		public boolean isUserID(String ID) {
			boolean found = false;
			found = managementDB.isAuthenticatePassword(ID);
			if(found) 
				setID(ID);
			else
				found = false;
			return found;
		}
		
		//Check Password
		public boolean isPassword(String password) {
			boolean found = false;
			//found = managementDB.isAuthenticatePassword(password); 
			if(managementDB.isAuthenticatePassword(password)) {
				setPassword(password); 
				found = true;
			}
			else
				found = false;
			return found;
		}
		
		//Return true if management staff is authenticated
		public String find(boolean username, boolean password){
			String status;
			status = (username == true && password == true)? String.format("Welcome %s",getID()):String.format("%s Authentication failed", "Login");		
			return status;			
		}
}
