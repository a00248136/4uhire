package com.ait.admin;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class RetrieveVehicle {
		
	public List<Vehicle> searchAll(){
	    ArrayList<Vehicle> results = new ArrayList<Vehicle>();
	    for(int i = 0; i < Vehicle.vehicles.size(); i++){
	            results.add(Vehicle.vehicles.get(i));
	    }
	    return results;
	}//end searchAll 
	
	public void searchByPrice(double minPrice, double maxPrice){
	    ArrayList<Vehicle> results = new ArrayList();
	    for(int i = 0; i < Vehicle.vehicles.size(); i++){
	       // if(vehicles.get(i).getRentingPrice() < maxPrice && vehicles.get(i).getRentingPrice() > minPrice){
	            results.add(Vehicle.vehicles.get(i));
	       // }
	    }
	}//end searchByPrice
	
	public List<Vehicle> searchByMake(String make){
	    ArrayList<Vehicle> results = new ArrayList();
	    for(int i = 0; i < Vehicle.vehicles.size(); i++){
	        //if(vehicles.get(i).getMake().equalsIgnoreCase(make)){
	            results.add(Vehicle.vehicles.get(i));
	       //}
	    }
	    return results;
	}//end searchByMake 
	
	public List<Vehicle> searchByModel(String model){
	    ArrayList<Vehicle> results = new ArrayList();
	    for(int i = 0; i < Vehicle.vehicles.size(); i++){
	        //if(vehicles.get(i).getModel().equalsIgnoreCase(model)){
	            results.add(Vehicle.vehicles.get(i));
	       //}
	    }
	    return results;
	}//end searchByModel
	

	public List<Vehicle> searchByYear(int year){
	    ArrayList<Vehicle> results = new ArrayList<Vehicle>();
	    for(int i = 0; i < Vehicle.vehicles.size(); i++){
	       // if(vehicles.get(i).getYear() == year){
	            results.add(Vehicle.vehicles.get(i));
	       // }
	    }
	    return results;
	}//end searchByYear
	

	
	public void displaySearchResults(ArrayList<Vehicle> results){
	    for(Vehicle vehicle : results){
	        //Display results in labels on html
	    }
	}	
}