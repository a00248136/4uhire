package com.ait.admin;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class Vehicle
{
	static ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
	
	
	private String registration;
	private String model;
	private String make;
	private int doorSize;
	private double engineSize;
	private String category;
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public int getDoorSize() {
		return doorSize;
	}

	public void setDoorSize(int doorSize) {
		this.doorSize = doorSize;
	}

	public double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(double engineSize) {
		this.engineSize = engineSize;
	}

	public String getRegistration() {
		return registration;
	}

	public Vehicle setRegistration(String registration) {
		this.registration = registration;
		return this;
	}

	public String getCategory() {
		return category;
	}

	public Vehicle setCategory(String category) {
		this.category = category;
		return this;
	}
	/*
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);

		
	    // Create an array list vehicles
	    ArrayList<String> vehicles = new ArrayList<String>();

		// Program should run until choice isn't equal to "y" or "Y"
        String choice = "y";

        while (choice.equalsIgnoreCase("y"))
        {    		
    		// Create an object v
    		Vehicle v = new Vehicle();

			System.out.print("Enter registration: ");
			String newRegistration = sc.next();

			System.out.print("Enter brand: ");
			String newBrand = sc.next();

			System.out.print("Enter name: ");
			String newName = sc.next();

			System.out.print("Enter vehcile category: ");
			String newCategory = sc.next();

			v.setRegistration(newRegistration);
			v.getModel(newBrand);
			v.getMake(newName);
			v.setCategory(newCategory);

			vehicles.add(v.getRegistration());
			vehicles.add(v.getModel());
			vehicles.add(v.getMake());
			vehicles.add(v.getCategory());

			// see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            

            System.out.println();
        }
    	// Display overall results if user chooses not to continue
        if (choice.equalsIgnoreCase("n"))
        {
    		// Display results to user
        	System.out.println(vehicles);
        }
	}
	*/
}