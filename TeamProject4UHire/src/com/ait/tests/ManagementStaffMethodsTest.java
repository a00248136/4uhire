package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.ManagementStaff;
import com.ait.admin.ManagementStaffMethods;

public class ManagementStaffMethodsTest {

	private ManagementStaffMethods managementStaffMethods;
	
	@Before
	public void setup(){
		managementStaffMethods = new ManagementStaffMethods();
	}
	
	@Test
	public void testInputDateOfBirth() {
		managementStaffMethods.setInputDateOfBirth("01/01/2011");
		assertEquals("01/01/2011", managementStaffMethods.getInputDateOfBirth());	
	}
	
	@Test
	public void testInputAddress() {
		managementStaffMethods.setInputAddress("Athlone");
		assertEquals("Athlone", managementStaffMethods.getInputAddress());	
	}
	
	@Test
	public void testInputGender() {
		managementStaffMethods.setInputGender("Male");
		assertEquals("Male", managementStaffMethods.getInputGender());	
	}
	
	@Test
	public void testIncorrectEntry() {
		managementStaffMethods.setIncorrectEntry("IncorrectEntry");
		assertEquals("IncorrectEntry", managementStaffMethods.getIncorrectEntry());	
	}
	
	@Test
	public void testInputFirstName() {
		managementStaffMethods.setInputFirstName("FirstName");
		assertEquals("FirstName", managementStaffMethods.getInputFirstName());	
	}
	
	@Test
	public void testInputLastName() {
		managementStaffMethods.setInputLastName("LastName");
		assertEquals("LastName", managementStaffMethods.getInputLastName());	
	}
	
	@Test
	public void testInputPassword() {
		managementStaffMethods.setInputPassword("newPassword");
		assertEquals("newPassword", managementStaffMethods.getInputPassword());	
	}
	
	@Test
	public void testWrongUsername() {
		managementStaffMethods.setWrongUsername("wrongUsername");
		assertEquals("wrongUsername", managementStaffMethods.getWrongUsername());	
	}
	
	@Test
	public void testAddManagementStaff() {
		ManagementStaff ms = new ManagementStaff("John", "Doe", "Male", "johnDoe", "Athlone", 
				"01/01/2011", "myPassword");
		ms.setUsername("John");
		managementStaffMethods.setInputUsername("John");
		managementStaffMethods.addManagementStaff();
		assertEquals("This Username is already taken", managementStaffMethods.getWrongUsername());	
	}
}