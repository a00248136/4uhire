package com.ait.tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import com.ait.admin.ManagementStaff;

public class ManagementStaffTest {

private ManagementStaff managementStaff;
	
	@Before
	public void setup(){
		managementStaff = new ManagementStaff("John", "Doe", "Male", "johnDoe", "Athlone"
				, "01/01/2011", "myPassword");
	}
	
	@Test
	public void testGender() {
		assertEquals("Male", managementStaff.getGender());
		managementStaff.setGender("Female");
		assertEquals("Female", managementStaff.getGender());	
	}
	
	@Test
	public void testFirstName() {
		assertEquals("John", managementStaff.getFirstName());
		managementStaff.setFirstName("Steven");
		assertEquals("Steven", managementStaff.getFirstName());	
	}
	
	@Test
	public void testLastName() {
		assertEquals("Doe", managementStaff.getLastName());
		managementStaff.setLastName("King");
		assertEquals("King", managementStaff.getLastName());	
	}
	
	@Test
	public void testUsername() {
		assertEquals("johnDoe", managementStaff.getUsername());
		managementStaff.setUsername("newUsername");
		assertEquals("newUsername", managementStaff.getUsername());	
	}
	
	@Test
	public void testAddress() {
		assertEquals("Athlone", managementStaff.getAddress());
		managementStaff.setAddress("Galway");
		assertEquals("Galway", managementStaff.getAddress());	
	}
	
	@Test
	public void tesDateOfBirth() {
		assertEquals("01/01/2011", managementStaff.getDateOfBirth());
		managementStaff.setDateOfBirth("02/02/2012");
		assertEquals("02/02/2012", managementStaff.getDateOfBirth());	
	}
	
	@Test
	public void testPassword() {
		assertEquals("myPassword", managementStaff.getPassword());
		managementStaff.setPassword("newPassword");
		assertEquals("newPassword", managementStaff.getPassword());	
	}
}