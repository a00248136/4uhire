package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.User;

public class UserTest {

private User user;
	
	@Before
	public void setup(){
		user = new User("myUsername", "myPassword");
	}
	
	@Test
	public void testUsername() {
		assertEquals("myUsername", user.getUserName());
		user.setUserName("newUsername");
		assertEquals("newUsername", user.getUserName());	
	}
	
	@Test
	public void testPassword() {
		assertEquals("myPassword", user.getPassword());
		user.setPassword("newPassword");
		assertEquals("newPassword", user.getPassword());	
	}

}
