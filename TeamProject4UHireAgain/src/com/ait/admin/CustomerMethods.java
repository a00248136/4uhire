package com.ait.admin;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class CustomerMethods {
	private String incorrectEntry;
	private String inputFirstName;
	private String inputLastName;
	private String inputPassword;
	private String inputUsername;
	private String inputDateOfBirth;
	private String inputAddress;
	private String inputGender;
	private String inputLicenceNumber;
	private String inputLicenceExpiry;
	private String inputEmail;
	private String wrongUsername = "";
	
	private ArrayList<Customer> customers;
	
	@PostConstruct
	public void init(){
		customers=new ArrayList<Customer>();
		
	}

	public String getIncorrectEntry() {
		return incorrectEntry;
	}

	public void setIncorrectEntry(String incorrectEntry) {
		this.incorrectEntry = incorrectEntry;
	}

	public String getInputFirstName() {
		return inputFirstName;
	}

	public void setInputFirstName(String inputFirstName) {
		this.inputFirstName = inputFirstName;
	}

	public String getInputLastName() {
		return inputLastName;
	}

	public void setInputLastName(String inputLastName) {
		this.inputLastName = inputLastName;
	}

	public String getInputPassword() {
		return inputPassword;
	}

	public void setInputPassword(String inputPassword) {
		this.inputPassword = inputPassword;
	}

	public String getInputUsername() {
		return inputUsername;
	}

	public void setInputUsername(String inputUsername) {
		this.inputUsername = inputUsername;
	}

	public String getInputDateOfBirth() {
		return inputDateOfBirth;
	}

	public void setInputDateOfBirth(String inputDateOfBirth) {
		this.inputDateOfBirth = inputDateOfBirth;
	}

	public String getInputAddress() {
		return inputAddress;
	}

	public void setInputAddress(String inputAddress) {
		this.inputAddress = inputAddress;
	}

	public String getInputGender() {
		return inputGender;
	}

	public void setInputGender(String inputGender) {
		this.inputGender = inputGender;
	}

	public String getInputLicenceNumber() {
		return inputLicenceNumber;
	}

	public void setInputLicenceNumber(String inputLicenceNumber) {
		this.inputLicenceNumber = inputLicenceNumber;
	}

	public String getInputLicenceExpiry() {
		return inputLicenceExpiry;
	}

	public void setInputLicenceExpiry(String inputLicenceExpiry) {
		this.inputLicenceExpiry = inputLicenceExpiry;
	}

	public String getInputEmail() {
		return inputEmail;
	}

	public void setInputEmail(String inputEmail) {
		this.inputEmail = inputEmail;
	}

	public String getWrongUsername() {
		return wrongUsername;
	}

	public void setWrongUsername(String wrongUsername) {
		this.wrongUsername = wrongUsername;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}
	public String addCustomer(){
		boolean alreadyExist=false;
		for(Customer cs:customers){
			if(cs.getUsername().equals(inputUsername)){
				alreadyExist=true;
				break;
			}
		}
		if(alreadyExist){
			wrongUsername="This Username is already taken";
		}else{
			Customer cust=new Customer(inputFirstName,inputLastName,inputGender,inputUsername,inputAddress,inputDateOfBirth,inputPassword,inputLicenceNumber,inputLicenceExpiry,inputEmail);
			customers.add(cust);
			inputFirstName = null;
			inputLastName = null;
			inputUsername = null;
			inputPassword = null;
			inputAddress=null;
			inputGender=null;
			inputDateOfBirth=null;
			inputLicenceExpiry=null;
			inputLicenceNumber=null;
			inputEmail=null;
			wrongUsername = "";
			return "CustomerAccount.jsf";
		}
		return null;
	}
	public String login() {
 		for (Customer c : customers) {
 			if (c.getUsername().equals(inputUsername)&& c.getPassword().equals(inputPassword)) {
 				incorrectEntry = "";
				return "CustomerBookVehicle.jsf";		
			}
 		}
 	incorrectEntry = "Enter the correct username or password!";
	inputUsername = "";
	return "";


	
	


	}

	

}

