package com.ait.admin;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LoginBean {
	private String username;
	private String password;
	private boolean loggedIn;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getPassword() {
		return password;
	}
	
	public boolean getLoggedIn() {
		return loggedIn;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String login() {
		System.out.println("login called");
		UserData userData=Helper.getBean("userData", UserData.class);
		ArrayList<User> users=userData.getUsers();
		for (User user:users){
			if(user.getUserName().equals(username)){
				if(user.getPassword().equals(password)){
					loggedIn = true;
					return "superAdmin.jsf";
				}
			}
		}
		//FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,
		//		"Incorrect Username and Passowrd",
		//		"Please enter correct username and Password"));
		loggedIn = false;
		return "login.jsf";
	}

}