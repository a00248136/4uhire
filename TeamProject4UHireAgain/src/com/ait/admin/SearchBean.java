package com.ait.admin;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped

//this class is used to check through the ArrayList 'vehicles' in the VehicleMethods Class in order to add matching vehicles (by dailyPrice) to the ArrayList 'vehiclesOfPrice'.
//the resulting ArrayList is then used to populate the table in the xhtml page.

public class SearchBean{
	public ArrayList<Vehicle> searchResults = new ArrayList<Vehicle>(); // this ArrayList will be used to populate the table displaying results of the price search
	static Vehicle matchingVehicle = null; // holder object which will be used to populate the 'searchResults' ArrayList
	private static double enteredPrice; //// The input from the user is bound to this variable (in the xhtml file 'searchByPrice') and used in the searchByPrice method below to compare to the VehicleMethod arraylist
	
	// These member variables are static as the scope of the class is Request (one-off instantiation per request)
	
	public ArrayList<Vehicle> getSearchResults() {
		return searchResults;
	}
	
		
	public void setSearchResults(ArrayList<Vehicle> searchResults) {
		this.searchResults = searchResults;
	}


	public double getEnteredPrice() {
		return enteredPrice;
	}
	
	public void setEnteredPrice(double enteredPrice) {
		SearchBean.enteredPrice = enteredPrice;
	}

	
	public void searchByPrice(){
		VehicleMethods tempVehicles = Helper.getBean("vehicleMethods", VehicleMethods.class);
		ArrayList<Vehicle> defaultVehiclesSearch = tempVehicles.getDefaultVehicles(); // Two ArrayLists are required to check the defaultVehicles plus any vehicles that may be added during the session
		ArrayList<Vehicle> vehiclesSearch = tempVehicles.getVehicles();
		
		System.out.println("SearchResults reached");
		
		
		if (enteredPrice > 0 && enteredPrice <= 100){
			for(Vehicle searcher: defaultVehiclesSearch){ // The first search will perform the search through the 'defaultVehicles' ArrayList
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 0 && comparePrice <= 100){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){ // The second search will perform the search through the 'vehicles' ArrayList (which are session-bound/instantiated)
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 0 && comparePrice <= 100){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
		
		else if(enteredPrice > 100 && enteredPrice <= 200){
			for(Vehicle searcher: defaultVehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 100 && comparePrice <= 200){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 100 && comparePrice <= 200){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
		
		else if(enteredPrice > 200 && enteredPrice <= 300){
			for(Vehicle searcher: defaultVehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 200 && comparePrice <= 300){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 200 && comparePrice <= 300){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
		
		else if(enteredPrice > 300 && enteredPrice <= 400){
			for(Vehicle searcher: defaultVehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 300 && comparePrice <= 400){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 300 && comparePrice <= 400){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
		
		else if(enteredPrice > 400 && enteredPrice <= 500){
			for(Vehicle searcher: defaultVehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 400 && comparePrice <= 500){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 400 && comparePrice <= 500){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
		
		else if(enteredPrice > 500 && enteredPrice <= 600){
			for(Vehicle searcher: defaultVehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 500 && comparePrice <= 600){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
			for(Vehicle searcher: vehiclesSearch){
				double comparePrice = searcher.getRentalPrice();
		
				if(comparePrice > 500 && comparePrice <= 600){
					matchingVehicle = searcher;
					searchResults.add(matchingVehicle);
				}
			}
		}
	}
}