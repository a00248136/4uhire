//package com.ait.admin;
//
//import java.util.ArrayList;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;
//import java.io.Serializable;
//
//@ManagedBean
//@SessionScoped

//this class is used to check through the ArrayList 'vehicles' in the VehicleMethods Class in order to add matching vehicles (by dailyPrice) to the ArrayList 'vehiclesOfPrice'.
//the resulting ArrayList is then used to populate the table in the xhtml page.

//public class SearchVehicleByPrice implements Serializable{
//	private static final long serialVersionUID = 1L;
//	
//	static ArrayList<Vehicle> vehiclesOfPrice = new ArrayList<Vehicle>();
//	
//	private double enteredPrice;

//
//	public void searchByPrice(String enteredPrice){
//		Vehicle matchingVehicle = null;
//	
//		if (enteredPrice > 0 && enteredPrice <= 100){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 0 && comparePrice <= 100){
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//				}
//			}
//		}
//		
//		else if(enteredPrice > 100 && enteredPrice <= 200){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 100 && comparePrice <= 200)
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//			}
//		}
//		
//		else if(enteredPrice > 200 && enteredPrice <= 300){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 200 && comparePrice <= 300)
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//			}
//		}
//		
//		else if(enteredPrice > 300 && enteredPrice <= 400){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 300 && comparePrice <= 400)
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//			}
//		}
//		
//		else if(enteredPrice > 400 && enteredPrice <= 500){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 400 && comparePrice <= 500)
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//			}
//		}
//		
//		else if(enteredPrice > 500 && enteredPrice <= 600){
//			for(Vehicle searcher: VehicleMethods.vehicles){
//				double comparePrice = searcher.getRentalPrice();
//		
//				if(comparePrice > 500 && comparePrice <= 600)
//					matchingVehicle = searcher;
//					vehiclesOfPrice.add(matchingVehicle);
//			}
//		}
//	}
//}