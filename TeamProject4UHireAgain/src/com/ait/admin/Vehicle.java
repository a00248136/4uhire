package com.ait.admin;

public class Vehicle {
	private String make;
	private String model;
	private String fuelType;
	private String registration;
	private double engineSize;
	private int doorNum;
	private double rentalPrice;
	boolean canEdit;
	private String image;
	
	public Vehicle() { // need a default constructor to allow for the use of the 'addVehicle' method in VehicleMethods class.
	} 
	
	public Vehicle(String make, String model, String fuelType, int doorNum, 
				double engineSize, String registration, double rentalPrice,String image) {
		this.make = make;
		this.model = model;
		this.fuelType = fuelType;
		this.doorNum = doorNum;
		this.engineSize= engineSize;
		this.registration = registration;
		this.rentalPrice = rentalPrice;
		this.image=image;
	}
	
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}


	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}


	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(double engineSize) {
		this.engineSize = engineSize;
	}

	public int getDoorNum() {
		return doorNum;
	}

	public void setDoorNum(int doorNum) {
		this.doorNum = doorNum;
	}

	public double getRentalPrice() {
		return rentalPrice;
	}

	public void setRentalPrice(double rentalPrice) {
		this.rentalPrice = rentalPrice;
	}
}