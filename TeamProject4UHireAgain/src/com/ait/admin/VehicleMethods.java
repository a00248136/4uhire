package com.ait.admin;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class VehicleMethods {

	public static ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
	private ArrayList<Vehicle> defaultVehicles = new ArrayList<Vehicle>();
	
	private String make;
	private String model;
	private String fuelType;
	private String registration;
	private double engineSize;
	private int doorNum;
	private double rentalPrice;
	private String image;

	@PostConstruct
	public void init() {
		defaultVehicles = new ArrayList<Vehicle>();
		Vehicle firstVehicle = new Vehicle("Rolls", "Royce", "Petrol", 5, 4.5, "10G1234", 500.0,"rolls.jpg");
		defaultVehicles.add(firstVehicle);
		Vehicle secondVehicle = new Vehicle("Mercedes", "m3", "Diesel", 3, 1.8, "08C3487", 250.0,"merc.png");
		defaultVehicles.add(secondVehicle);
		Vehicle thirdVehicle = new Vehicle("BMW", "b4", "Petrol", 4, 1.6, "06L9283", 220.0,"bmw6.png");


		defaultVehicles.add(thirdVehicle);
	}

	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}
	
	public ArrayList<Vehicle> getDefaultVehicles() {
		return defaultVehicles;
	}
	
	public static void setVehicles(ArrayList<Vehicle> vehicles) {
		VehicleMethods.vehicles = vehicles;
	}

	public void setDefaultVehicles(ArrayList<Vehicle> defaultVehicles) {
		this.defaultVehicles = defaultVehicles;
	}

//	public String deleteVehicle(Vehicle vehicle) {
//		vehicles.remove(vehicle);
//		return null;
//	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(double engineSize) {
		this.engineSize = engineSize;
	}

	public int getDoorNum() {
		return doorNum;
	}

	public void setDoorNum(int doorNum) {
		this.doorNum = doorNum;
	}

	public double getRentalPrice() {
		return rentalPrice;
	}

	public void setRentalPrice(double rentalPrice) {
		this.rentalPrice = rentalPrice;
	}
	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}


	public  String addVehicle(){ 
		Vehicle newVehicle= new Vehicle(make, model, fuelType, doorNum,
				engineSize, registration, rentalPrice,image);
		vehicles.add(newVehicle);
		//these lines are here so the text field is cleared
		make=null;
		model=null;
		fuelType=null;
		doorNum = 0;
		engineSize = 0.0;
		registration=null;
		rentalPrice = 0.0;
		return null;
	}
} 