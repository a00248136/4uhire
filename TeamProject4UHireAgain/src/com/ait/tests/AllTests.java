package com.ait.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CreateCustomerTest.class, CustomerMethodsTest.class, HelperTest.class, LoginBeanTest.class,
		ManagementStaffMethodsTest.class, ManagementStaffTest.class, UserDataTest.class, UserTest.class,
		VehicleMethodsTest.class, VehicleTest.class })
public class AllTests {

}
