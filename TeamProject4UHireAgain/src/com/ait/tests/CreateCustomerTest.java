package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.Customer;

public class CreateCustomerTest {

	private Customer customer;
	
	@Before
    public void setUp() {
		customer = new Customer("Tom", "Steven", "Male", "root", "Galway", "28/8/1976", "admin", "tom@example.com", "161G257", "30/6/2018");
	}
	
	@Test
	public void testVehicleConstructor() {
		assertEquals("root", customer.getUsername());
		assertEquals("admin", customer.getPassword());
		assertEquals("Tom", customer.getFirstName());
		assertEquals("Steven", customer.getLastName());
		assertEquals("28/8/1976", customer.getDateOfBirth());
		assertEquals("Male", customer.getGender());
		assertEquals("Galway", customer.getAddress());
		assertEquals("tom@example.com", customer.getEmail());
		assertEquals("161G257", customer.getLicenceNumber());
		assertEquals("30/6/2018", customer.getLicenceExpiry());
	}

	@Test
	public void testUserName() {
		assertEquals("root", customer.getUsername());
		customer.setUsername("Tom173");
		assertEquals("Tom173", customer.getUsername());
	}
	
	@Test
	public void testPassword() {
		assertEquals("admin", customer.getPassword());
		customer.setPassword("hrte731");
		assertEquals("hrte731", customer.getPassword());
	}
	
	@Test
	public void testFirstName() {
		assertEquals("Tom", customer.getFirstName());
		customer.setFirstName("Sarah");
		assertEquals("Sarah", customer.getFirstName());
	}
	
	@Test
	public void testLastName() {
		assertEquals("Steven", customer.getLastName());
		customer.setLastName("Buckley");
		assertEquals("Buckley", customer.getLastName());
	}
	
	@Test
	public void testDateOfBirth() {
		assertEquals("28/8/1976", customer.getDateOfBirth());
		customer.setDateOfBirth("19/9/1968");
		assertEquals("19/9/1968", customer.getDateOfBirth());
	}
	
	@Test
	public void testGender() {
		assertEquals("Male", customer.getGender());
		customer.setGender("Female");
		assertEquals("Female", customer.getGender());
	}
	
	@Test
	public void testAddress() {
		assertEquals("Galway", customer.getAddress());
		customer.setAddress("Dublin");
		assertEquals("Dublin", customer.getAddress());
	}
	
	@Test
	public void testEmail() {
		assertEquals("tom@example.com", customer.getEmail());
		customer.setEmail("sarah@gmail.com");
		assertEquals("sarah@gmail.com", customer.getEmail());
	}
	
	@Test
	public void testLicenceNumber() {
		assertEquals("161G257", customer.getLicenceNumber());
		customer.setLicenceNumber("171D95836");
		assertEquals("171D95836", customer.getLicenceNumber());
	}
	
	@Test
	public void testLicenceExpiry() {
		assertEquals("30/6/2018", customer.getLicenceExpiry());
		customer.setLicenceExpiry("31/5/2019");
		assertEquals("31/5/2019", customer.getLicenceExpiry());
	}
}
