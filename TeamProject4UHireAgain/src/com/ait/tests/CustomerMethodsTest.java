package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.Customer;
import com.ait.admin.CustomerMethods;

public class CustomerMethodsTest {
	
	CustomerMethods customerMethods;
	Customer customer;
	
	@Before
	public void setup() {
		customerMethods = new CustomerMethods();
	}
	
	@Test
	public void testInputUsername() {
		customerMethods.setInputUsername("root");
		assertEquals("root", customerMethods.getInputUsername());
	}
	
	@Test
	public void testInputPassword() {
		customerMethods.setInputPassword("admin");
		assertEquals("admin", customerMethods.getInputPassword());
	}

	@Test
	public void testInputFirstName() {
		customerMethods.setInputFirstName("Tom");
		assertEquals("Tom", customerMethods.getInputFirstName());
	}

	@Test
	public void testInputLastName() {
		customerMethods.setInputLastName("Steven");
		assertEquals("Steven", customerMethods.getInputLastName());
	}
	
	@Test
	public void testInputDateOfBirth() {
		customerMethods.setInputDateOfBirth("28/8/1976");
		assertEquals("28/8/1976", customerMethods.getInputDateOfBirth());
	}
	
	@Test
	public void testInputGender() {
		customerMethods.setInputGender("Male");
		assertEquals("Male", customerMethods.getInputGender());
	}
	
	@Test
	public void testInputAddress() {
		customerMethods.setInputAddress("Galway");
		assertEquals("Galway", customerMethods.getInputAddress());
	}
	
	@Test
	public void testInputEmail() {
		customerMethods.setInputEmail("tom@example.com");
		assertEquals("tom@example.com", customerMethods.getInputEmail());
	}
	
	@Test
	public void testInputLicenceNumber() {
		customerMethods.setInputLicenceNumber("161G257");
		assertEquals("161G257", customerMethods.getInputLicenceNumber());
	}
	
	@Test
	public void testInputLicenceExpiry() {
		customerMethods.setInputLicenceExpiry("30/6/2018");
		assertEquals("30/6/2018", customerMethods.getInputLicenceExpiry());
	}
	
	@Test
	public void testGetCustomerArray() {
		customerMethods.getCustomers();
	}
	
	@Test
	public void testWrongUsername() {
		customerMethods.setWrongUsername(null);
		customerMethods.getWrongUsername();
	}
		
	@Test
	public void testAddCustomer() {
		customerMethods.init();
		customerMethods.setInputUsername("username");
		customerMethods.setInputAddress("Athlone");
		customerMethods.setInputDateOfBirth("01/01/2017");
		customerMethods.setInputFirstName("John");
		customerMethods.setInputGender("Male");
		customerMethods.setInputLastName("Doe");
		customerMethods.setInputPassword("password");
		assertEquals("CustomerAccount.jsf", customerMethods.addCustomer());
		Customer customer1 = new Customer("John", "Doe", "Male", "username", "Athlone", 
				"01/01/2017", "password", "johndoe@ait.ie", "sdf67sd", "01/01/2020");
		customerMethods.setInputUsername(customer1.getUsername());
		customerMethods.setInputAddress(customer1.getAddress());
		customerMethods.setInputDateOfBirth(customer1.getDateOfBirth());
		customerMethods.setInputFirstName(customer1.getFirstName());
		customerMethods.setInputGender(customer1.getGender());
		customerMethods.setInputLastName(customer1.getLastName());
		customerMethods.setInputPassword(customer1.getPassword());
		assertNull(customerMethods.addCustomer());
		assertEquals("This Username is already taken", customerMethods.getWrongUsername()); 
	}
	
	@Test
	public void testLogin() {
		customerMethods.init();
		customerMethods.setInputUsername("username");
		customerMethods.setInputAddress("Athlone");
		customerMethods.setInputDateOfBirth("01/01/2017");
		customerMethods.setInputFirstName("John");
		customerMethods.setInputGender("Male");
		customerMethods.setInputLastName("Doe");
		customerMethods.setInputPassword("password");
		assertEquals("CustomerAccount.jsf", customerMethods.addCustomer());
		Customer customer1 = new Customer("John", "Doe", "Male", "username", "Athlone", 
				"01/01/2017", "password", "johndoe@ait.ie", "sdf67sd", "01/01/2020");
		customerMethods.setInputUsername(customer1.getUsername());
		customerMethods.setInputAddress(customer1.getAddress());
		customerMethods.setInputDateOfBirth(customer1.getDateOfBirth());
		customerMethods.setInputFirstName(customer1.getFirstName());
		customerMethods.setInputGender(customer1.getGender());
		customerMethods.setInputLastName(customer1.getLastName());
		customerMethods.setInputPassword(customer1.getPassword());
		customerMethods.login();	
		assertEquals("CustomerBookVehicle.jsf", customerMethods.login());
	}
	
	@Test
	public void testInvalidLogin() {
		customerMethods.init();
		customerMethods.login();	
		assertEquals("Enter the correct username or password!", customerMethods.getIncorrectEntry());
		customerMethods.setIncorrectEntry("wrongEntry");
		assertEquals("wrongEntry", customerMethods.getIncorrectEntry());
	}
}