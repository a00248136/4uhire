package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.LoginBean;
import com.ait.admin.UserData;

public class LoginBeanTest {

	private LoginBean loginBean;
	
	@Before
	public void setup(){
		loginBean = new LoginBean();
	}

	@Test
	public void testGetAdminUsernamePassword() {
		loginBean.setUsername("root");
		loginBean.setPassword("superadmin");
		assertEquals("root", loginBean.getUsername());
		assertEquals("superadmin", loginBean.getPassword());	
	}
	
	@Test
	public void testSetAdminUsernamePassword() {
		loginBean.setUsername("Steven");
		assertEquals("Steven", loginBean.getUsername());
		loginBean.setPassword("newPassword");
		assertEquals("newPassword", loginBean.getPassword());	
	}
	
	@Test
	public void testAdminValidLogin() {
		loginBean.setUsername("root");
		loginBean.setPassword("superadmin");
		loginBean.setLoggedIn(true);
		assertTrue(loginBean.getLoggedIn());
	}
	
}