package com.ait.tests;

import static org.junit.Assert.*;


import java.util.ArrayList;


import org.junit.Before;
import org.junit.Test;

import com.ait.admin.ManagementStaff;
import com.ait.admin.ManagementStaffMethods;

public  class ManagementStaffMethodsTest {
	
	private ManagementStaffMethods managementStaffMethods;
	private ManagementStaff staff;
	
	
	
	@Before
	public void setup(){
		
		//managementStaffs = new ArrayList<ManagementStaff>(5);
		managementStaffMethods = new ManagementStaffMethods();
		staff = new ManagementStaff("John", "Doe", "Male", "lancs", "Athlone", 
				"01/01/2011", "myPassword");	
	}
	@Test
	public void testInputDateOfBirth() {
		managementStaffMethods.setInputDateOfBirth("01/01/2011");
		assertEquals("01/01/2011", managementStaffMethods.getInputDateOfBirth());	
	}
	
	@Test
	public void testInputAddress() {
		managementStaffMethods.setInputAddress("Athlone");
		assertEquals("Athlone", managementStaffMethods.getInputAddress());	
	}
	
	@Test
	public void testInputGender() {
		managementStaffMethods.setInputGender("Male");
		assertEquals("Male", managementStaffMethods.getInputGender());	
	}
	
	@Test
	public void testIncorrectEntry() {
		managementStaffMethods.setIncorrectEntry("IncorrectEntry");
		assertEquals("IncorrectEntry", managementStaffMethods.getIncorrectEntry());	
	}
	
	@Test
	public void testInputFirstName() {
		managementStaffMethods.setInputFirstName("FirstName");
		assertEquals("FirstName", managementStaffMethods.getInputFirstName());	
	}
	
	@Test
	public void testInputLastName() {
		managementStaffMethods.setInputLastName("LastName");
		assertEquals("LastName", managementStaffMethods.getInputLastName());	
	}
	
	@Test
	public void testInputPassword() {
		managementStaffMethods.setInputPassword("newPassword");
		assertEquals("newPassword", managementStaffMethods.getInputPassword());	
	}
	
	@Test
	public void testWrongUsername() {
		managementStaffMethods.setWrongUsername("wrongUsername");
		assertEquals("wrongUsername", managementStaffMethods.getWrongUsername());	
	}
	
	@Test
	public void testAddManagementStaff() {
		managementStaffMethods.init();
		managementStaffMethods.setInputUsername(staff.getUsername());
		managementStaffMethods.setInputAddress(staff.getAddress());
		managementStaffMethods.setInputDateOfBirth(staff.getDateOfBirth());
		managementStaffMethods.setInputFirstName(staff.getFirstName());
		managementStaffMethods.setInputGender(staff.getGender());
		managementStaffMethods.setInputLastName(staff.getLastName());
		managementStaffMethods.setInputPassword(staff.getPassword());
		assertEquals("show-registration.jsf", managementStaffMethods.addManagementStaff());
		ManagementStaff staff2 = new ManagementStaff("John", "Doe", "Male", "lancs", "Athlone", 
				"01/01/2011", "myPassword");
		managementStaffMethods.setInputUsername(staff2.getUsername());
		managementStaffMethods.setInputAddress(staff2.getAddress());
		managementStaffMethods.setInputDateOfBirth(staff2.getDateOfBirth());
		managementStaffMethods.setInputFirstName(staff2.getFirstName());
		managementStaffMethods.setInputGender(staff2.getGender());
		managementStaffMethods.setInputLastName(staff2.getLastName());
		managementStaffMethods.setInputPassword(staff2.getPassword());		
		assertNull(managementStaffMethods.addManagementStaff());
		assertEquals("This Username is already taken", managementStaffMethods.getWrongUsername());
		
	}
	@Test
	public void testIsAuthenticateUsername() {	
		managementStaffMethods.init();
		managementStaffMethods.setInputUsername(staff.getUsername());
		managementStaffMethods.setInputAddress(staff.getAddress());
		managementStaffMethods.setInputDateOfBirth(staff.getDateOfBirth());
		managementStaffMethods.setInputFirstName(staff.getFirstName());
		managementStaffMethods.setInputGender(staff.getGender());
		managementStaffMethods.setInputLastName(staff.getLastName());
		managementStaffMethods.setInputPassword(staff.getPassword());
		managementStaffMethods.addManagementStaff();
		assertTrue(managementStaffMethods.isAuthenticateUsername("lancs"));
		assertFalse(managementStaffMethods.isAuthenticateUsername("fred"));
	}	
	
	@Test
	public void testIsAuthenticatePassword() {
		managementStaffMethods.init();
		managementStaffMethods.setInputUsername(staff.getUsername());
		managementStaffMethods.setInputAddress(staff.getAddress());
		managementStaffMethods.setInputDateOfBirth(staff.getDateOfBirth());
		managementStaffMethods.setInputFirstName(staff.getFirstName());
		managementStaffMethods.setInputGender(staff.getGender());
		managementStaffMethods.setInputLastName(staff.getLastName());
		managementStaffMethods.setInputPassword(staff.getPassword());
		managementStaffMethods.addManagementStaff();
		assertTrue(managementStaffMethods.isAuthenticatePassword("myPassword"));
		assertFalse(managementStaffMethods.isAuthenticatePassword("MYPASSWORD"));		
	}
	
	@Test
	public void testFindStaff() {
		managementStaffMethods.init();
		managementStaffMethods.setInputUsername(staff.getUsername());
		assertEquals("lancs", managementStaffMethods.getInputUsername());
		managementStaffMethods.setInputAddress(staff.getAddress());
		managementStaffMethods.setInputDateOfBirth(staff.getDateOfBirth());
		managementStaffMethods.setInputFirstName(staff.getFirstName());
		managementStaffMethods.setInputGender(staff.getGender());
		managementStaffMethods.setInputLastName(staff.getLastName());
		managementStaffMethods.setInputPassword(staff.getPassword());
		managementStaffMethods.addManagementStaff();
		
		managementStaffMethods.setInputUsername("lancs");
		managementStaffMethods.setInputPassword("myPassword");
		String message = managementStaffMethods.findStaff();
		assertEquals("ManagerAddVehicles.jsf", message);
		managementStaffMethods.setInputUsername("maven");
		managementStaffMethods.setInputPassword("myPassword");
		message = managementStaffMethods.findStaff();
		assertEquals("MangerLogin.jsf", message);
	}	
}