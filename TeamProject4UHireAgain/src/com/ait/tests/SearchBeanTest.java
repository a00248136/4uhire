package com.ait.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.ait.admin.SearchBean;
import com.ait.admin.Vehicle;



public class SearchBeanTest {
	private static SearchBean searchTest = new SearchBean();

	@Test
	public void testGetSearchResults() {
		searchTest.setSearchResults(null);
		assertEquals(null, searchTest.getSearchResults());
	}
	
	@Test
	public void testSetSearchResults() {
		SearchBean searchTestTemp = new SearchBean();
		searchTestTemp.setSearchResults(null);
		assertEquals(null, searchTestTemp.getSearchResults());
	}
	
	@Test
	public void testGetEnteredPrice() {
		searchTest.getEnteredPrice();
		assertEquals(0, 0, searchTest.getEnteredPrice());
		//need to repeat the double parameter to avoid the deprecated assert warning (precision is never required to more than 2 digits for the 4U Hire system)
	}
	
	@Test
	public void testSetEnteredPrice() {
		searchTest.setEnteredPrice(199.99);
		assertEquals(199.99, 199.99, searchTest.getEnteredPrice());
		//need to repeat the double parameter to avoid the deprecated assert warning (precision is never required to more than 2 digits for the 4U Hire system)
	}
	
//	@Test
//	public void testSearchByPriceOutcomeOne() {
//		SearchBean searchTestTemp2 = new SearchBean();
//		searchTestTemp2.setEnteredPrice(99.99);
//		
//		searchTestTemp2.searchByPrice();
//		assertEquals(searchTestTemp2.searchResults.size(), 1); 
//	}

	
}
