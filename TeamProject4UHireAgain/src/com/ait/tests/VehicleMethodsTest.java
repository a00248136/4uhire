package com.ait.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.Vehicle;
import com.ait.admin.VehicleMethods;

public class VehicleMethodsTest {
	
	VehicleMethods vehicleMethods;
	
	@Before
    public void setUp() {
		vehicleMethods = new VehicleMethods();
	}

	@Test
	public void testMake() {
		vehicleMethods.setMake("Opel");
		assertEquals("Opel", vehicleMethods.getMake());
	}
	
	@Test
	public void testModel() {
		vehicleMethods.setModel("Zafira");
		assertEquals("Zafira", vehicleMethods.getModel());
	}
	
	@Test
	public void testDoorNum() {
		vehicleMethods.setDoorNum(7);
		assertEquals(7, vehicleMethods.getDoorNum());
	}
	
	@Test
	public void testEngineSize() {
		vehicleMethods.setEngineSize(1.9);
		assertEquals(1.9, vehicleMethods.getEngineSize(), 0.0);
	}
	
	@Test
	public void testFuelType() {
		vehicleMethods.setFuelType("Diesel");
		assertEquals("Diesel", vehicleMethods.getFuelType());
	}
	
	@Test
	public void testRegistration() {
		vehicleMethods.setRegistration("142D62436");
		assertEquals("142D62436", vehicleMethods.getRegistration());
	}
	
	@Test
	public void testRentalPrice() { 
		vehicleMethods.setRentalPrice(89.99);
		assertEquals(89.99, vehicleMethods.getRentalPrice(), 0.0);
	}
	
	@Test
	public void  testImageChanged(){
		vehicleMethods.setImage("bent.jpg");
		assertEquals("bent.jpg",vehicleMethods.getImage());
	} 
	
	@Test
	public void  testArrayListVehicles(){
	    assertEquals(vehicleMethods.vehicles.size(), 0); 
	    assertEquals(null, vehicleMethods.addVehicle());
	}
	
	@Test
	public void  testGetVehicleArray(){
		vehicleMethods.setVehicles(null);
		vehicleMethods.getVehicles();
		
		vehicleMethods.setDefaultVehicles(null);
		vehicleMethods.getDefaultVehicles();
	}
	
	@Test
	public void  testGetDefaultVehicles(){
		ArrayList<Vehicle> sizeTester = vehicleMethods.getDefaultVehicles();
		assertEquals(sizeTester.size(), 0); 
	}
	
	@Test
	public void  testInit(){
		vehicleMethods.init();
		ArrayList<Vehicle> sizeTester = vehicleMethods.getDefaultVehicles();
		assertEquals(sizeTester.size(), 3); 
	}
}