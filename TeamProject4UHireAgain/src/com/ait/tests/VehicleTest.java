package com.ait.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ait.admin.Vehicle;

public class VehicleTest {

	private Vehicle vehicle;
	
	@Before
    public void setUp() {
		vehicle = new Vehicle("Ford", "Focus", "Petrol", 5, 1.6, "06G28345", 48.99,"beetle.jpg");
	}
	
	@Test
	public void testVehicleConstructor() {
		assertEquals("Ford", vehicle.getMake());
		assertEquals("Focus", vehicle.getModel());
		assertEquals("Petrol", vehicle.getFuelType());
		assertEquals(5, vehicle.getDoorNum());
		assertEquals(1.6, vehicle.getEngineSize(), 0.0);
		assertEquals("06G28345", vehicle.getRegistration());
		assertEquals(48.99, vehicle.getRentalPrice(), 0.0);
	}
	
	@Test
	public void testMake() {
		assertEquals("Ford", vehicle.getMake());
		vehicle.setMake("Opel");
		assertEquals("Opel", vehicle.getMake());
	}
	
	@Test
	public void testModel() {
		assertEquals("Focus", vehicle.getModel());
		vehicle.setModel("Zafira");
		assertEquals("Zafira", vehicle.getModel());
	}
	
	@Test
	public void testDoorNum() {
		assertEquals(5, vehicle.getDoorNum());
		vehicle.setDoorNum(7);
		assertEquals(7, vehicle.getDoorNum());
	}
	
	@Test
	public void testEngineSize() {
		assertEquals(1.6, vehicle.getEngineSize(), 0.0);
		vehicle.setEngineSize(1.9);
		assertEquals(1.9, vehicle.getEngineSize(), 0.0);
	}
	
	@Test
	public void testFuelType() {
		assertEquals("Petrol", vehicle.getFuelType());
		vehicle.setFuelType("Diesel");
		assertEquals("Diesel", vehicle.getFuelType());
	}
	
	@Test
	public void testRegistration() {
		assertEquals("06G28345", vehicle.getRegistration());
		vehicle.setRegistration("142D62436");
		assertEquals("142D62436", vehicle.getRegistration());
	}
	
	@Test
	public void setRentalPrice() {
		assertEquals(48.99, vehicle.getRentalPrice(), 0.0);
		vehicle.setRentalPrice(89.99);
		assertEquals(89.99, vehicle.getRentalPrice(), 0.0);
	}
	@Test
	public void  testImageChanged(){
		vehicle.setImage("bent.jpg");
		assertEquals("bent.jpg",vehicle.getImage());
	}
}